# Grigosoft React DatePickers

## Introduction

Grigosoft React DateTimePickers is a library of components for React 18+ styled with Tailwind.

:warning: This library is still an early WIP state.

### Roadmap

- [x] TimePicker Component
- [x] Styling option for TimePicker component
- [x] Add this "awesome" readme
- [ ] DatePicker component
- [ ] Styling option for DatePicker component
- [ ] DateTimePicker component
- [ ] Styling option for DateTimePicker component
- [ ] Providing better usefull examples for each component usage



## Timepicker props

| Parameter           | Type                                         | Description                                                                                                   |
| :------------------ | :------------------------------------------- | :------------------------------------------------------------------------------------------------------------ |
| `date`              | `Date`                                     | Provides the Timepicker initial value. It is required to control the state of the component.                                               |
| `setDate`              | `(date:Date)=>void`                                     | Provide a methos to update the initial value. It is required to control the state of the component.                                               |
| `disabled`               | `bool`                                     | disabled flag for the component. The Timepicker will be disabled.          |
| `maxDate?`             | `Date`                                     | Upper limit for the timepicker valid values                |
| `minDate?`             | `Date`                                     | Lower limit for the timepicker valid values                |
| `baseClassName?`           | `(isValid: boolean, isDisabled?: boolean) => string`                                 |  Allows to provide a function to override the default style of the root component of the Timepicker|
| `inputClassName?`           | `(isDisabled?: boolean) => string`                                 | Allows to provide a function to override the default style of the input components in the Timepicker |
| `modalClassNames?`           | `ModalClassNames`                                 | Allows to provide a specific function to override the default style of the modals components for the hour, minute and second fields of the Timepicker |
| `optionClassNames?`           | `OptionClassNames`                                 | Allows to provide a the style for the Option components in the Timepicker modals. It is possible to specify a complete style or the customize the single Disabled,Selected and Hover styles of the Options |

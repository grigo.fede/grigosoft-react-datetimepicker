import React, { useState } from 'react';
import { TimePicker } from '@grigosoft/react-datetimepicker';

function App() {
    const [date, setdate] = useState(new Date());
    const [date2, setdate2] = useState(new Date());
    return (
        <div className='bg-background h-screen '>
            <TimePicker date={date2} setDate={setdate2} />
            <TimePicker date={date} setDate={setdate} minDate={date2} />
            <TimePicker date={date} setDate={setdate} disabled />
        </div>
    );
}

export default App;

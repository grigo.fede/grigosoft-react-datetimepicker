export declare const _short_hours: number[];
export declare const _hours: number[];
export declare const _minutes: number[];
export declare const _seconds: number[];
interface CalendarMonth {
    name: string;
    shortName: string;
}
interface CalendarDay {
    name: string;
    shortName: string;
}
export declare const translateMonths: {
    [id: number]: CalendarMonth;
};
export declare const translateDays: {
    [id: number]: CalendarDay;
};
export {};

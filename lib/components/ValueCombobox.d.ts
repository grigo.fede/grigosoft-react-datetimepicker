/// <reference types="react" />
import { Option } from '../types/structs';
export interface ValueComboboxProps {
    keyPrefix: string;
    open: boolean;
    showDivider?: boolean;
    setOpen: (val: boolean) => void;
    disabled?: boolean;
    query: string;
    value: Option;
    handleSelection: (option: Option) => void;
    handleQuery: (value: string) => void;
    options: Option[];
    cellValidator: (value: any) => boolean;
    reset: () => void;
    inputClass?: (disabled?: boolean) => string;
    modalClass?: (disabled?: boolean) => string;
    optionClasses?: {
        classNames?: ({ disabled, selected, active }: {
            disabled: boolean;
            selected: boolean;
            active: boolean;
        }) => string;
        selectedClassname?: (selected: boolean) => string;
        activeClassname?: (active: boolean) => string;
        disabledClassname?: (disabled: boolean) => string;
        hoverClassname?: string;
    };
}
declare function ValueCombobox(props: ValueComboboxProps): JSX.Element;
export default ValueCombobox;

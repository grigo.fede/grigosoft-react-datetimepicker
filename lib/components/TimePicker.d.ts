/// <reference types="react" />
import '../themes/index.css';
export interface ModalClassNames {
    hours: (isDisabled?: boolean) => string;
    minutes: (isDisabled?: boolean) => string;
    seconds: (isDisabled?: boolean) => string;
}
export interface OptionClassNames {
    classNames?: ({ disabled, selected, active }: {
        disabled: boolean;
        selected: boolean;
        active: boolean;
    }) => string;
    selectedClassname?: (selected: boolean) => string;
    activeClassname?: (active: boolean) => string;
    disabledClassname?: (disabled: boolean) => string;
    hoverClassname?: string;
}
export interface TimePickerProps {
    date?: Date;
    disabled?: boolean;
    setDate?: (date: Date) => void;
    maxDate?: Date;
    minDate?: Date;
    baseClassName?: (isValid: boolean, isDisabled?: boolean) => string;
    modalClassNames?: ModalClassNames;
    inputClassName?: (isDisabled?: boolean) => string;
    optionClassNames?: OptionClassNames;
}
declare function TimePicker(props: TimePickerProps): JSX.Element;
export default TimePicker;

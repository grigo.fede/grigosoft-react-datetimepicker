import React from 'react';
import { Option } from '../types/structs';
export interface TimePickerOptionProps {
    option: Option;
    disabled?: boolean;
    optionKey: string;
    classNames?: ({ disabled, selected, active }: {
        disabled: boolean;
        selected: boolean;
        active: boolean;
    }) => string;
    selectedClassname?: (selected: boolean) => string;
    activeClassname?: (active: boolean) => string;
    disabledClassname?: (disabled: boolean) => string;
    hoverClassname?: string;
}
declare function TimePickerOption(props: TimePickerOptionProps): JSX.Element;
declare const _default: React.MemoExoticComponent<typeof TimePickerOption>;
export default _default;

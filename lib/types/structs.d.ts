export declare type Option = {
    name: string;
    label?: string;
    value: any;
};

import React, { Component, memo } from 'react';

import { Combobox } from '@headlessui/react';
import { Option } from '../types/structs';

export interface TimePickerOptionProps {
    option: Option;
    disabled?: boolean;
    optionKey: string;
    classNames?: ({
        disabled,
        selected,
        active
    }: {
        disabled: boolean;
        selected: boolean;
        active: boolean;
    }) => string;
    selectedClassname?: (selected: boolean) => string;
    activeClassname?: (active: boolean) => string;
    disabledClassname?: (disabled: boolean) => string;
    hoverClassname?: string;
}

function TimePickerOption(props: TimePickerOptionProps) {
    const {
        disabled,
        option,
        classNames,
        activeClassname,
        disabledClassname,
        selectedClassname,
        hoverClassname
    } = props;

    //methods

    const classes =
        classNames ||
        (({ disabled, selected, active }) =>
            `block select-none  px-0.5 text-center ${
                hoverClassname
                    ?.split(' ')
                    .filter(ss => ss != '')
                    .join(' hover:') || 'hover:bg-green-500 hover:text-white'
            } 
            ${
                disabledClassname
                    ? disabledClassname(disabled)
                    : disabled
                    ? 'bg-gray-200 opacity-60'
                    : 'cursor-pointer'
            } 
            ${
                selectedClassname
                    ? selectedClassname(selected)
                    : selected
                    ? 'bg-green-200 font-semibold'
                    : 'font-normal'
            } 
            ${activeClassname ? activeClassname(active) : active ? '' : ''}`);

    return (
        <Combobox.Option
            disabled={disabled}
            key={'key' + (props.optionKey || option.value)}
            className={classes}
            value={option}>
            {option.name}
        </Combobox.Option>
    );
}

export default memo(TimePickerOption, (prev, next) => {
    if (prev.disabled === next.disabled && prev.option === next.option) return true;
    return false;
});

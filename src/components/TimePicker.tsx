import { Combobox, Transition } from '@headlessui/react';
import { Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { Option } from '../types/structs';

import '../themes/index.css';
import { _hours, _minutes, _seconds } from '../config/constants';
import useOnClickOutside from '../hooks/useOnClickOutside';
import TimePickerOption from './TimePickerOption';
import ValueCombobox from './ValueCombobox';

let hours: number[] = [];
let minsec: number[] = [];
for (let i = 0; i < 60; i++) {
    minsec.push(i);
    if (i < 24) hours.push(i);
}

export interface ModalClassNames {
    hours: (isDisabled?: boolean) => string;
    minutes: (isDisabled?: boolean) => string;
    seconds: (isDisabled?: boolean) => string;
}

export interface OptionClassNames {
    classNames?: ({
        disabled,
        selected,
        active
    }: {
        disabled: boolean;
        selected: boolean;
        active: boolean;
    }) => string;
    selectedClassname?: (selected: boolean) => string;
    activeClassname?: (active: boolean) => string;
    disabledClassname?: (disabled: boolean) => string;
    hoverClassname?: string;
}

export interface TimePickerProps {
    date?: Date;
    disabled?: boolean;
    setDate?: (date: Date) => void;
    maxDate?: Date;
    minDate?: Date;
    baseClassName?: (isValid: boolean, isDisabled?: boolean) => string;
    modalClassNames?: ModalClassNames;
    inputClassName?: (isDisabled?: boolean) => string;
    optionClassNames?: OptionClassNames;
}

function TimePicker(props: TimePickerProps) {
    //refs
    const pickerRef = useRef<any>();

    // state
    const [showModalState, setShowModalState] = useState(false);

    const [queries, setQueries] = useState({
        hours: '',
        minutes: '',
        seconds: ''
    });

    const [filteredLists, setFilteredList] = useState<{
        hours: Option[];
        minutes: Option[];
        seconds: Option[];
    }>({
        hours: [..._hours.map<Option>(val => ({ name: val.toString(), value: val }))],
        minutes: [..._minutes.map<Option>(val => ({ name: val.toString(), value: val }))],
        seconds: [..._seconds.map<Option>(val => ({ name: val.toString(), value: val }))]
    });

    const [dataValue, setDataValue] = useState(new Date());

    // hooks
    useOnClickOutside(pickerRef, () => {
        resetOnClose();
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const _getDate = useCallback(() => (props.date ? props.date : dataValue), [props.date]);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const _setDate = useCallback(props.setDate ? props.setDate : setDataValue, [props.setDate]);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const _isValid = useCallback(() => {
        return props.minDate
            ? _getDate().getTime() >= props.minDate.getTime()
            : true && props.maxDate
            ? _getDate().getTime() <= props.maxDate.getTime()
            : true;
    }, [_getDate, props.maxDate, props.minDate]);

    const _wouldBeValid = useCallback(
        (type: 'hours' | 'minutes' | 'seconds', val: number) => {
            const t = new Date(_getDate());

            switch (type) {
                case 'hours': {
                    t.setHours(val);
                    break;
                }
                case 'minutes': {
                    t.setMinutes(val);
                    break;
                }
                case 'seconds': {
                    t.setSeconds(val);
                    break;
                }
            }

            return props.minDate
                ? t.getTime() >= props.minDate.getTime()
                : true && props.maxDate
                ? t.getTime() <= props.maxDate.getTime()
                : true;
        },
        [_getDate, props.maxDate, props.minDate]
    );

    // methods

    const _makeOptionFromNumber = (num: number): Option => ({
        value: num,
        name: num > 9 ? num.toString() : `0${num}`
    });

    const resetOnClose = () => {
        setQueries({
            hours: '',
            minutes: '',
            seconds: ''
        });
        setFilteredList({
            hours: [..._hours.map<Option>(_makeOptionFromNumber)],
            minutes: [..._minutes.map<Option>(_makeOptionFromNumber)],
            seconds: [..._seconds.map<Option>(_makeOptionFromNumber)]
        });
        setShowModalState(false);
    };

    const handleInput = (type: 'hours' | 'minutes' | 'seconds', value: string) => {
        switch (type) {
            case 'hours': {
                setFilteredList({
                    ...filteredLists,
                    hours: [
                        ..._hours
                            .filter(
                                hour =>
                                    value === '' ||
                                    hour.toString().includes(parseInt(value).toString())
                            )
                            .map<Option>(_makeOptionFromNumber)
                    ]
                });
                setQueries({ ...queries, hours: value });
                break;
            }
            case 'minutes': {
                setFilteredList({
                    ...filteredLists,
                    minutes: [
                        ..._minutes
                            .filter(
                                minute =>
                                    value === '' ||
                                    minute.toString().includes(parseInt(value).toString())
                            )
                            .map<Option>(_makeOptionFromNumber)
                    ]
                });
                setQueries({ ...queries, minutes: value });
                break;
            }
            case 'seconds': {
                setFilteredList({
                    ...filteredLists,
                    seconds: [
                        ..._seconds
                            .filter(
                                second =>
                                    value === '' ||
                                    second.toString().includes(parseInt(value).toString())
                            )
                            .map<Option>(_makeOptionFromNumber)
                    ]
                });
                setQueries({ ...queries, seconds: value });
            }
        }
    };

    const handleSelection = (type: 'hours' | 'minutes' | 'seconds', value: Option) => {
        switch (type) {
            case 'hours': {
                const tmp = new Date(_getDate());
                tmp.setHours(value.value);
                _setDate(tmp);
                break;
            }
            case 'minutes': {
                const tmp = new Date(_getDate());
                tmp.setMinutes(value.value);
                _setDate(tmp);
                break;
            }
            case 'seconds': {
                const tmp = new Date(_getDate());
                tmp.setSeconds(value.value);
                _setDate(tmp);
            }
        }
    };

    // render

    const defaultBaseClass =
        props.baseClassName ||
        ((isValid: boolean, disabled?: boolean) =>
            `relative inline-flex rounded-md border border-black px-1 py-0.5  ${
                isValid ? 'bg-white' : 'bg-red-200'
            } ${disabled ? 'text-opacity-50' : ''}`);

    return (
        <>
            <span ref={pickerRef} className={defaultBaseClass(_isValid(), props.disabled)}>
                <ValueCombobox
                    keyPrefix={'hours'}
                    open={showModalState}
                    setOpen={setShowModalState}
                    query={queries.hours}
                    handleQuery={(val: string) => {
                        handleInput('hours', val);
                    }}
                    handleSelection={(option: Option) => handleSelection('hours', option)}
                    reset={resetOnClose}
                    disabled={props.disabled}
                    value={{ value: _getDate().getHours(), name: _getDate().getHours().toString() }}
                    options={filteredLists.hours}
                    cellValidator={(val: any) => _wouldBeValid('hours', val)}
                    inputClass={
                        props.inputClassName ||
                        ((disabled?: boolean) => {
                            return `w-8 text-base `;
                        })
                    }
                    modalClass={
                        props.modalClassNames?.hours ||
                        ((disabled?: boolean) => {
                            return 'text-base border-t border-b border-l border-black rounded-l-lg shadow-lg ';
                        })
                    }
                    optionClasses={
                        props.optionClassNames || {
                            selectedClassname: (selected: boolean) =>
                                selected ? 'bg-green-200 font-semibold' : 'font-normal',
                            activeClassname: (active: boolean) => '',
                            disabledClassname: (disabled: boolean) =>
                                disabled ? 'bg-gray-200 opacity-60' : 'cursor-pointer',
                            hoverClassname: 'hover:bg-green-500 text-white'
                        }
                    }
                />

                <ValueCombobox
                    keyPrefix={'minutes'}
                    showDivider
                    open={showModalState}
                    setOpen={setShowModalState}
                    query={queries.minutes}
                    handleQuery={(val: string) => {
                        handleInput('minutes', val);
                    }}
                    handleSelection={(option: Option) => handleSelection('minutes', option)}
                    reset={resetOnClose}
                    disabled={props.disabled}
                    value={{
                        value: _getDate().getMinutes(),
                        name: _getDate().getMinutes().toString()
                    }}
                    options={filteredLists.minutes}
                    cellValidator={(val: any) => _wouldBeValid('minutes', val)}
                    inputClass={
                        props.inputClassName ||
                        ((disabled?: boolean) => {
                            return `w-8 text-base `;
                        })
                    }
                    modalClass={
                        props.modalClassNames?.minutes ||
                        ((disabled?: boolean) => {
                            return 'text-base border border-black shadow-lg border-l-gray-400 border-r-gray-400';
                        })
                    }
                    optionClasses={
                        props.optionClassNames || {
                            selectedClassname: (selected: boolean) =>
                                selected ? 'bg-green-200 font-semibold' : 'font-normal',
                            activeClassname: (active: boolean) => '',
                            disabledClassname: (disabled: boolean) =>
                                disabled ? 'bg-gray-200 opacity-60' : 'cursor-pointer',
                            hoverClassname: 'hover:bg-green-500 text-white'
                        }
                    }
                />

                <ValueCombobox
                    keyPrefix={'seconds'}
                    showDivider
                    open={showModalState}
                    setOpen={setShowModalState}
                    query={queries.seconds}
                    handleQuery={(val: string) => {
                        handleInput('seconds', val);
                    }}
                    handleSelection={(option: Option) => handleSelection('seconds', option)}
                    reset={resetOnClose}
                    disabled={props.disabled}
                    value={{
                        value: _getDate().getSeconds(),
                        name: _getDate().getSeconds().toString()
                    }}
                    options={filteredLists.seconds}
                    cellValidator={(val: any) => _wouldBeValid('seconds', val)}
                    inputClass={
                        props.inputClassName ||
                        ((disabled?: boolean) => {
                            return `w-8 text-base `;
                        })
                    }
                    modalClass={
                        props.modalClassNames?.seconds ||
                        ((disabled?: boolean) => {
                            return 'text-base border-t border-r border-b rounded-r-lg border-black shadow-lg ';
                        })
                    }
                    optionClasses={
                        props.optionClassNames || {
                            selectedClassname: (selected: boolean) =>
                                selected ? 'bg-green-200 font-semibold' : 'font-normal',
                            activeClassname: (active: boolean) => '',
                            disabledClassname: (disabled: boolean) =>
                                disabled ? 'bg-gray-200 opacity-60' : 'cursor-pointer',
                            hoverClassname: 'hover:bg-green-500 text-white'
                        }
                    }
                />
            </span>
        </>
    );
}

export default TimePicker;

import { Combobox, Transition } from '@headlessui/react';
import { Fragment } from 'react';
import { Option } from '../types/structs';
import TimePickerOption from './TimePickerOption';

export interface ValueComboboxProps {
    keyPrefix: string;
    open: boolean;
    showDivider?: boolean;
    setOpen: (val: boolean) => void;
    disabled?: boolean;
    query: string;
    value: Option;
    handleSelection: (option: Option) => void;
    handleQuery: (value: string) => void;
    options: Option[];
    cellValidator: (value: any) => boolean;
    reset: () => void;
    inputClass?: (disabled?: boolean) => string;
    modalClass?: (disabled?: boolean) => string;
    optionClasses?: {
        classNames?: ({
            disabled,
            selected,
            active
        }: {
            disabled: boolean;
            selected: boolean;
            active: boolean;
        }) => string;
        selectedClassname?: (selected: boolean) => string;
        activeClassname?: (active: boolean) => string;
        disabledClassname?: (disabled: boolean) => string;
        hoverClassname?: string;
    };
}

function ValueCombobox(props: ValueComboboxProps) {
    const {
        open,
        setOpen,
        handleQuery,
        handleSelection,
        query,
        value,
        disabled,
        options,
        cellValidator,
        reset,
        inputClass,
        modalClass,
        showDivider,
        keyPrefix
    } = props;

    const defaultInputClass =
        inputClass || ((disabled?: boolean) => `w-7 text-sm ${disabled ? '' : ''}`);
    const defaultModalClass = modalClass || (() => `border bg-white text-sm`);

    return (
        <Combobox
            key={keyPrefix}
            disabled={disabled}
            value={value}
            by={'value'}
            onChange={handleSelection}>
            <div className='relative'>
                <div className='relative cursor-default overflow-hidden px-0 text-left focus:outline-none focus:ring-0'>
                    {showDivider && <span>:</span>}
                    <Combobox.Input
                        className={`border-none bg-transparent p-0 text-center focus:ring-0 ${defaultInputClass(
                            disabled
                        )} `}
                        displayValue={(num: any) => (num.value < 10 ? '0' + num.name : num.name)}
                        value={query}
                        onChange={event => {
                            if (!isNaN(event.target.value as any)) handleQuery(event.target.value);
                        }}
                        type='text'
                        maxLength={2}
                        pattern='^[0-9]*$'
                        min={0}
                        max={24}
                        onClick={() => {
                            if (!open && !props.disabled) setOpen(true);
                        }}
                        onKeyDownCapture={(e: any) => {
                            if (e.key === 'Enter') {
                                if (open) reset();
                            }
                        }}
                    />
                </div>
                <Transition
                    as={Fragment}
                    show={open}
                    leave='transition ease-in duration-100'
                    leaveFrom='opacity-100'
                    leaveTo='opacity-0'
                    afterLeave={() => {
                        handleQuery('');
                    }}>
                    <Combobox.Options
                        className={`no-scrollbar absolute z-10 mt-1 h-max max-h-48 min-h-[192px] w-full overflow-auto ${defaultModalClass(
                            disabled
                        )}`}>
                        {options.length === 0 && query !== '' ? (
                            <div className='relative select-none  '></div>
                        ) : (
                            options.map(entry => (
                                <TimePickerOption
                                    key={`${keyPrefix}_${entry.value}`}
                                    option={entry}
                                    optionKey={`${keyPrefix}_${entry.value}`}
                                    disabled={!cellValidator(entry.value)}
                                    {...props.optionClasses}
                                />
                            ))
                        )}
                    </Combobox.Options>
                </Transition>
            </div>
        </Combobox>
    );
}

export default ValueCombobox;

export const _short_hours: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
export const _hours: number[] = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23
];
export const _minutes: number[] = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
    50, 51, 52, 53, 54, 55, 56, 57, 58, 59
];
export const _seconds: number[] = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
    50, 51, 52, 53, 54, 55, 56, 57, 58, 59
];

interface CalendarMonth {
    name: string;
    shortName: string;
}

interface CalendarDay {
    name: string;
    shortName: string;
}

export const translateMonths: { [id: number]: CalendarMonth } = {
    0: {
        name: 'date.months.january',
        shortName: 'date.months.january-short'
    },
    1: {
        name: 'date.months.february',
        shortName: 'date.months.february-short'
    },
    2: {
        name: 'date.months.march',
        shortName: 'date.months.march-short'
    },
    3: {
        name: 'date.months.april',
        shortName: 'date.months.april-short'
    },
    4: {
        name: 'date.months.may',
        shortName: 'date.months.may-short'
    },
    5: {
        name: 'date.months.june',
        shortName: 'date.months.june-short'
    },
    6: {
        name: 'date.months.july',
        shortName: 'date.months.july-short'
    },
    7: {
        name: 'date.months.august',
        shortName: 'date.months.august-short'
    },
    8: {
        name: 'date.months.september',
        shortName: 'date.months.september-short'
    },
    9: {
        name: 'date.months.october',
        shortName: 'date.months.october-short'
    },
    10: {
        name: 'date.months.november',
        shortName: 'date.months.november-short'
    },
    11: {
        name: 'date.months.december',
        shortName: 'date.months.december-short'
    }
};

export const translateDays: { [id: number]: CalendarDay } = {
    0: {
        name: 'date.days.sunday',
        shortName: 'date.days.sunday-short'
    },
    1: {
        name: 'date.days.monday',
        shortName: 'date.days.monday-short'
    },
    2: {
        name: 'date.days.tuesday',
        shortName: 'date.days.tuesday-short'
    },
    3: {
        name: 'date.days.wednesday',
        shortName: 'date.days.wednesday-short'
    },
    4: {
        name: 'date.days.thursday',
        shortName: 'date.days.thursday-short'
    },
    5: {
        name: 'date.days.friday',
        shortName: 'date.days.friday-short'
    },
    6: {
        name: 'date.days.saturday',
        shortName: 'date.days.saturday-short'
    }
};

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
    darkMode: 'media',
    mode: 'jit',
    theme: {
        extend: {
            transitionProperty: {
                height: 'height',
                maxHeight: 'max-height',
                spacing: 'margin, padding'
            }
        }
    },
    plugins: [require('@tailwindcss/typography'), require('@tailwindcss/forms')]
};
